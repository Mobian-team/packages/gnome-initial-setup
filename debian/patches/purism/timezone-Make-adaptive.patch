From: Alexander Mikhaylenko <alexander.mikhaylenko@puri.sm>
Date: Thu, 22 Apr 2021 12:16:22 +0500
Subject: timezone: Make adaptive

---
 .../pages/timezone/gis-timezone-page.c             | 21 ++++++++++++
 .../pages/timezone/gis-timezone-page.ui            | 37 +++++++++++++++++++---
 2 files changed, 54 insertions(+), 4 deletions(-)

diff --git a/gnome-initial-setup/pages/timezone/gis-timezone-page.c b/gnome-initial-setup/pages/timezone/gis-timezone-page.c
index 5db5413..bcb33cc 100644
--- a/gnome-initial-setup/pages/timezone/gis-timezone-page.c
+++ b/gnome-initial-setup/pages/timezone/gis-timezone-page.c
@@ -66,6 +66,7 @@ static void stop_geolocation (GisTimezonePage *page);
 
 struct _GisTimezonePagePrivate
 {
+  GtkWidget *swindow;
   GtkWidget *map;
   GtkWidget *search_entry;
   GtkWidget *search_overlay;
@@ -503,6 +504,7 @@ gis_timezone_page_class_init (GisTimezonePageClass *klass)
 
   gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (klass), "/org/gnome/initial-setup/gis-timezone-page.ui");
 
+  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisTimezonePage, swindow);
   gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisTimezonePage, map);
   gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisTimezonePage, search_entry);
   gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisTimezonePage, search_overlay);
@@ -514,9 +516,24 @@ gis_timezone_page_class_init (GisTimezonePageClass *klass)
   object_class->dispose = gis_timezone_page_dispose;
 }
 
+static gboolean
+policy_for_small_screen (GBinding     *binding,
+                         const GValue *from_value,
+                         GValue       *to_value,
+                         gpointer      user_data)
+{
+  gboolean small_screen = g_value_get_boolean (from_value);
+
+  g_value_set_enum (to_value, small_screen ? GTK_POLICY_NEVER : GTK_POLICY_AUTOMATIC);
+
+  return TRUE;
+}
+
 static void
 gis_timezone_page_init (GisTimezonePage *page)
 {
+  GisTimezonePagePrivate *priv = gis_timezone_page_get_instance_private (page);
+
   g_resources_register (timezone_get_resource ());
   g_resources_register (datetime_get_resource ());
   g_type_ensure (CC_TYPE_TIMEZONE_MAP);
@@ -525,6 +542,10 @@ gis_timezone_page_init (GisTimezonePage *page)
   g_type_ensure (GIS_TYPE_LOCATION_ENTRY);
 
   gtk_widget_init_template (GTK_WIDGET (page));
+
+  g_object_bind_property_full (page, "small-screen", priv->swindow, "vscrollbar-policy",
+                               G_BINDING_SYNC_CREATE, policy_for_small_screen,
+                               NULL, NULL, NULL);
 }
 
 GisPage *
diff --git a/gnome-initial-setup/pages/timezone/gis-timezone-page.ui b/gnome-initial-setup/pages/timezone/gis-timezone-page.ui
index 588ec41..7a24cf0 100644
--- a/gnome-initial-setup/pages/timezone/gis-timezone-page.ui
+++ b/gnome-initial-setup/pages/timezone/gis-timezone-page.ui
@@ -3,13 +3,22 @@
 <interface>
   <!-- interface-requires gtk+ 3.0 -->
   <template class="GisTimezonePage" parent="GisPage">
+    <!-- Drop the page borders to add them back to inner widgets to allow the map to reach the borders. -->
+    <property name="margin_start">0</property>
+    <property name="margin_end">0</property>
+    <property name="margin_bottom">0</property>
     <child>
       <object class="GtkBox" id="box">
         <property name="visible">True</property>
         <property name="can_focus">False</property>
         <property name="orientation">vertical</property>
-        <property name="halign">center</property>
         <property name="valign">fill</property>
+        <child>
+          <object class="HdyClamp">
+            <property name="visible">True</property>
+            <!-- Add the borders back to the map inside its scrolled window, so it can visually reach the window's border in case the window is too narrow. -->
+            <property name="margin_start">12</property>
+            <property name="margin_end">12</property>
         <child>
           <object class="GisPageHeader" id="header">
             <property name="visible">True</property>
@@ -19,28 +28,46 @@
             <property name="icon_name">find-location-symbolic</property>
             <property name="show_icon" bind-source="GisTimezonePage" bind-property="small-screen" bind-flags="invert-boolean|sync-create"/>
           </object>
+        </child>
+          </object>
         </child>
         <child>
           <object class="GtkBox" id="page_box">
             <property name="visible">True</property>
             <property name="margin_top">18</property>
-            <property name="valign">center</property>
             <property name="orientation">vertical</property>
             <property name="spacing">14</property>
+            <child>
+              <object class="HdyClamp">
+                <property name="visible">True</property>
+                <property name="margin_start">12</property>
+                <property name="margin_end">12</property>
             <child>
               <object class="GisLocationEntry" id="search_entry">
                 <property name="visible">True</property>
-                <property name="halign">center</property>
                 <property name="max-width-chars">55</property>
               </object>
             </child>
+              </object>
+            </child>
+            <child>
+              <object class="GtkScrolledWindow" id="swindow">
+                <property name="visible">True</property>
+                <property name="propagate-natural-width">True</property>
+                <property name="propagate-natural-height">True</property>
+                <property name="min-content-height">244</property>
+                <property name="vexpand">True</property>
             <child>
               <object class="GtkFrame" id="map_frame">
                 <property name="visible">True</property>
                 <property name="can_focus">False</property>
-                <property name="margin_bottom">18</property>
                 <property name="hexpand">True</property>
                 <property name="label_xalign">0</property>
+                <property name="halign">center</property>
+                <property name="valign">center</property>
+                <property name="margin_start">12</property>
+                <property name="margin_end">12</property>
+                <property name="margin_bottom">18</property>
                 <child>
                   <object class="GtkOverlay" id="map_overlay">
                     <property name="visible">True</property>
@@ -66,6 +93,8 @@
                 </child>
               </object>
             </child>
+              </object>
+            </child>
           </object>
         </child>
       </object>
